const items = require("./3-arrays-vitamins");


//    1. Get all items that are available 

const availableItems = items.filter((item)=>{
    return item.available;
});


console.log("1. Available Items:\n");
console.log(availableItems);
console.log("\n\n");

// 2. Get all items containing only Vitamin C.

const availableVitaminCItems = items.filter((item)=>{
    return ( item.available && item.contains === "Vitamin C");
});

console.log("2. Only Vitamin C Available Items:\n");
console.log(availableVitaminCItems);
console.log("\n\n");

// 3. Get all items containing Vitamin A.

const availableVitaminAItems = items.filter((item)=>{
    return ( item.available && item.contains.includes("Vitamin A"));
});

console.log("3. All items containing Vitamin A:\n");
console.log(availableVitaminAItems);
console.log("\n\n");

// 4. Group items based on the Vitamins that they contain

let fruitGroupsByVitamins = items.reduce((acc, cur) => {
    if (!cur.available) {
        return acc;
    }

    let fruit = cur.name;
    let vitamins = cur.contains.split(",");
    let innerAcc;

    if (vitamins.length > 1) {
        innerAcc = vitamins.reduce((acc2, vitamin) => {
            vitamin = vitamin.trim();
            return { ...acc2, [vitamin]: [...(acc2[vitamin] || []), fruit] }
        }, acc);

        return innerAcc;
    }

    return { ...acc, [cur.contains]: [...(acc[cur.contains] || []), fruit] }
}, {});

console.log("4. Item groups based on vitamins:\n");
console.log(fruitGroupsByVitamins);
console.log("\n\n");

//  5. Sort items based on number of Vitamins they contain.

const sortedItems = items.sort((item1, item2)=>{

    let vitaminsInItem1 = item1.available ? item1.contains.split(",").length :0;
    let vitaminsInItem2 = item2.available ? item2.contains.split(",").length :0;
    
    return vitaminsInItem2 - vitaminsInItem1;
    
    });

console.log("5. Sorted (descending order) Items based on number of vitamins:\n");
console.log(sortedItems);
console.log("\n\n");

    
    
    


